var GLUtils = (function () {
	//Récupere le contexte WebGL du canvas
	//WebGL est la version d'OpenGL pour le web.
	//Ce contexte va nous servir à communiquer avec la carte graphique.
	this.getGLContext = function (canvas) {
		try {
			return canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
		} catch (e) {
			return null;
		}
	}

	/**
	 * Cette fonction cree la matrice la plus importante du programme :
	 * la matrice de projection en perspective.
	 * une matrice de projection transforme un espace en N dimensions
	 * en un espace en N-1 dimensions. Par exemple, 
	 * il existe une matrice de projection qui peut transormer votre corps
	 * (en 3D) en votre ombre (en 2D). Ce que nous allons faire utilise ce 
	 * principe.
	 * Nous allons multiplier chaque point (en 3D = XYZ) du cube par cette matrice
	 * pour obtenir un point sur l'ecran (en 2D = XY) dont les coordonnées
	 * seront exprimés en pixels.
	 * Cette matrice de projection est dite "en perspective" car elle fait en sorte
	 * que les objet lointains apparaissent plus petits sur l'écran, comme IRL.
	 * Ce nest pas le cas de toutes les matrices de projection, par exemple, certains jeux
	 * dit en "2d isométrique" utilisent une matrice de projection orthographique
	 * Les parametres sont les suivants : largeur et hauteur de l'écran,
	 * field of view = champ de vision, profondeur de champ min et max.
	 */
	this.getPerspectiveMatrix = function(width, height, fov, near, far) {
		var fovRad = fov * Math.PI / 180,
			f = 1 / Math.tan(fovRad / 2),
			ar = width / height,
			rangeInv = 1 / (near - far);

		return [
			f / ar, 	0, 		0, 							0,
			0, 			f, 		0, 							0,
			0, 			0, 		(near + far) * rangeInv, 	-1,
			0, 			0, 		near * far * rangeInv * 2, 	0
		];
	}

	//cree une matrice de translation (deplacement rectiligne)
	//en 3D
	this.getTranslationMatrix = function (xT, yT, zT) {
		return [
			 1,  0,  0, 0,
			 0,  1,  0, 0,
			 0,  0,  1, 0,
			xT, yT, zT, 1
		];
	}

	//Cree une matrice de rotation sur l'axe X en 3D.
	this.getXRotationMatrix = function (angleInDegree) {
		return getXRotationMatrixRad(angleInDegree * Math.PI / 180);
	}

	//Idem, mais avec un angle en radian plutot qu'en degre.
	this.getXRotationMatrixRad = function(angleInRadian) {
		return [
			1, 						  0, 					   0, 0,
			0,  Math.cos(angleInRadian), Math.sin(angleInRadian), 0,
			0, -Math.sin(angleInRadian), Math.cos(angleInRadian), 0,
			0, 						  0, 					   0, 1
		];
	}

	//idem sur l'axe Y.
	this.getYRotationMatrix = function (angleInDegree) {
		return getYRotationMatrixRad(angleInDegree * Math.PI / 180);
	}

	this.getYRotationMatrixRad = function (angleInRadian) {
		return [
			Math.cos(angleInRadian), 0, -Math.sin(angleInRadian), 0,
			0, 1, 0, 0,
			Math.sin(angleInRadian), 0, Math.cos(angleInRadian), 0,
			0, 0, 0, 1
		];
	}

	//idem sur l'axe Z
	this.getZRotationMatrix = function (angleInDegree) {
		return getZRotationMatrixRad(angleInDegree * Math.PI / 180);
	}

	this.getZRotationMatrixRad = function (angleInRadian) {
		return [
			Math.cos(angleInRadian), Math.sin(angleInRadian), 0, 0,
			-Math.sin(angleInRadian), Math.cos(angleInRadian), 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1
		];
	}

	this.getScaleMatrix = function(x, y, z) {
		return [
			x, 0, 0, 0,
			0, y, 0, 0,
			0, 0, z, 0,
			0, 0, 0, 1
		];
	}

	this.getCameraMatrix = function(x, y, z) {
		return [
			 1,  0,  0,  0,
			 0,  1,  0,  0,
			 0,  0,  1,  0,
			-x, -y, -z,  1
		];
	}

	//la matrice d'identité : elle ne fait rien !
	//Soit A une matrice quelconque, et I la matrice d'identité,
	//A×I = A.
	//C'est légale du nombre 1(4×1=4), mais chez les matrices 
	//Ici, elle signifie "aucune transformation geometrique"
	this.getIdentityMatrix = function () {
		return [
			1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1
		];
	}

	 //Permet de multiplier deux matrices 4×4 entre elles.
	this.mat4x4Mul = function(A, B) {
		var a = A[0], b = A[1], c = A[2], d = A[3],
			result = new Array(16);

		result[0] = a*B[0] + b*B[4] + c*B[8]  + d*B[12];
		result[1] = a*B[1] + b*B[5] + c*B[9]  + d*B[13];
		result[2] = a*B[2] + b*B[6] + c*B[10] + d*B[14];
		result[3] = a*B[3] + b*B[7] + c*B[11] + d*B[15];

		a = A[4]; b = A[5]; c = A[6]; d = A[7];

		result[4] = a*B[0] + b*B[4] + c*B[8]  + d*B[12];
		result[5] = a*B[1] + b*B[5] + c*B[9]  + d*B[13];
		result[6] = a*B[2] + b*B[6] + c*B[10] + d*B[14];
		result[7] = a*B[3] + b*B[7] + c*B[11] + d*B[15];

		a = A[8]; b = A[9]; c = A[10]; d = A[11];

		result[8]  = a*B[0] + b*B[4] + c*B[8]  + d*B[12];
		result[9]  = a*B[1] + b*B[5] + c*B[9]  + d*B[13];
		result[10] = a*B[2] + b*B[6] + c*B[10] + d*B[14];
		result[11] = a*B[3] + b*B[7] + c*B[11] + d*B[15];

		a = A[12]; b = A[13]; c = A[14]; d = A[15];

		result[12] = a*B[0] + b*B[4] + c*B[8]  + d*B[12];
		result[13] = a*B[1] + b*B[5] + c*B[9]  + d*B[13];
		result[14] = a*B[2] + b*B[6] + c*B[10] + d*B[14];
		result[15] = a*B[3] + b*B[7] + c*B[11] + d*B[15];

		return result;
	}

	this.mulVectorMatrix = function(v, m) {
		var a = v[0], b = v[1], c = v[2], d = v[3];
		return [
			a*m[0]  + b*m[4]  + c*m[8]  + d*m[12],
			a*m[1]  + b*m[5]  + c*m[9]  + d*m[13],
			a*m[2]  + b*m[6]  + c*m[10] + d*m[14],
			a*m[3] + b*m[7] + c*m[11] + d*m[15] 
		];
	}

	this.homogenousToCarthesian = function(v) {
		return [v[0] / v[3], v[1] / v[3], v[2] / v[3]];
	}

	this.vec2MulScalar = function(v, s) {
		return [v[0]*s, v[1]*s];
	}

	this.vec3MulScalar = function(v, s) {
		return [v[0]*s, v[1]*s, v[2]*s];
	}

	this.vec4MulScalar = function(v, s) {
		return [v[0]*s, v[1]*s, v[2]*s, v[3]*s];
	}

	this.vec2AddScalar = function(v, s) {
		return [v[0]+s, v[1]+s];
	}

	this.vec3AddScalar = function(v, s) {
		return [v[0]+s, v[1]+s, v[2]+s];
	}

	this.vec4AddScalar = function(v, s) {
		return [v[0]+s, v[1]+s, v[2]+s, v[3]+s];
	}

	this.mulVectorArrayMatrix = function(va, m) {
		var a, b, c, d, i1, i2, i3, result = new Array(16);
		for (var i=0; i<va.length; i+=4) {
			a = va[i]; b = va[i1]; c = va[i2]; d = va[i3];
			result[i ] = a*m[0]  + a*m[1]  + a*m[2]  + a*m[3];
			result[i1] = b*m[4]  + b*m[5]  + b*m[6]  + b*m[7];
			result[i2] = c*m[8]  + c*m[9]  + c*m[10] + c*m[11];
			result[i3] = d*m[12] + d*m[13] + d*m[14] + d*m[15];
		}
		return result;
	}

	this.mat4x4Inv = function(m) {
		var inv = new Array(16), det, i;

	    inv[0] = m[5]  * m[10] * m[15] - 
	             m[5]  * m[11] * m[14] - 
	             m[9]  * m[6]  * m[15] + 
	             m[9]  * m[7]  * m[14] +
	             m[13] * m[6]  * m[11] - 
	             m[13] * m[7]  * m[10];

	    inv[4] =-m[4]  * m[10] * m[15] + 
	             m[4]  * m[11] * m[14] + 
	             m[8]  * m[6]  * m[15] - 
	             m[8]  * m[7]  * m[14] - 
	             m[12] * m[6]  * m[11] + 
	             m[12] * m[7]  * m[10];

	    inv[8] = m[4]  * m[9]  * m[15] - 
	             m[4]  * m[11] * m[13] - 
	             m[8]  * m[5]  * m[15] + 
	             m[8]  * m[7]  * m[13] + 
	             m[12] * m[5]  * m[11] - 
	             m[12] * m[7]  * m[9];

	    inv[12]=-m[4]  * m[9]  * m[14] + 
	             m[4]  * m[10] * m[13] +
	             m[8]  * m[5]  * m[14] - 
	             m[8]  * m[6]  * m[13] - 
	             m[12] * m[5]  * m[10] + 
	             m[12] * m[6]  * m[9];

	    inv[1] =-m[1]  * m[10] * m[15] + 
	             m[1]  * m[11] * m[14] + 
	             m[9]  * m[2]  * m[15] - 
	             m[9]  * m[3]  * m[14] - 
	             m[13] * m[2]  * m[11] + 
	             m[13] * m[3]  * m[10];

	    inv[5] = m[0]  * m[10] * m[15] - 
	             m[0]  * m[11] * m[14] - 
	             m[8]  * m[2]  * m[15] + 
	             m[8]  * m[3]  * m[14] + 
	             m[12] * m[2]  * m[11] - 
	             m[12] * m[3]  * m[10];

	    inv[9] =-m[0]  * m[9]  * m[15] + 
	             m[0]  * m[11] * m[13] + 
	             m[8]  * m[1]  * m[15] - 
	             m[8]  * m[3]  * m[13] - 
	             m[12] * m[1]  * m[11] + 
	             m[12] * m[3]  * m[9];

	    inv[13]= m[0]  * m[9]  * m[14] - 
	             m[0]  * m[10] * m[13] - 
	             m[8]  * m[1]  * m[14] + 
	             m[8]  * m[2]  * m[13] + 
	             m[12] * m[1]  * m[10] - 
	             m[12] * m[2]  * m[9];

	    inv[2] = m[1]  * m[6]  * m[15] - 
	             m[1]  * m[7]  * m[14] - 
	             m[5]  * m[2]  * m[15] + 
	             m[5]  * m[3]  * m[14] + 
	             m[13] * m[2]  * m[7]  - 
	             m[13] * m[3]  * m[6];

	    inv[6] =-m[0]  * m[6]  * m[15] + 
	             m[0]  * m[7]  * m[14] + 
	             m[4]  * m[2]  * m[15] - 
	             m[4]  * m[3]  * m[14] - 
	             m[12] * m[2]  * m[7]  + 
	             m[12] * m[3]  * m[6];

	    inv[10] = m[0] * m[5]  * m[15] - 
	              m[0] * m[7]  * m[13] - 
	              m[4] * m[1]  * m[15] + 
	              m[4] * m[3]  * m[13] + 
	              m[12]* m[1]  * m[7]  - 
	              m[12]* m[3]  * m[5];

	    inv[14] =-m[0] * m[5]  * m[14] + 
	              m[0] * m[6]  * m[13] + 
	              m[4] * m[1]  * m[14] - 
	              m[4] * m[2]  * m[13] - 
	              m[12]* m[1]  * m[6]  + 
	              m[12]* m[2]  * m[5];

	    inv[3] = -m[1] * m[6]  * m[11] + 
	              m[1] * m[7]  * m[10] + 
	              m[5] * m[2]  * m[11] - 
	              m[5] * m[3]  * m[10] - 
	              m[9] * m[2]  * m[7]  + 
	              m[9] * m[3]  * m[6];

	    inv[7] =  m[0] * m[6]  * m[11] - 
	              m[0] * m[7]  * m[10] - 
	              m[4] * m[2]  * m[11] + 
	              m[4] * m[3]  * m[10] + 
	              m[8] * m[2]  * m[7]  - 
	              m[8] * m[3]  * m[6];

	    inv[11] =-m[0] * m[5]  * m[11] + 
	              m[0] * m[7]  * m[9]  + 
	              m[4] * m[1]  * m[11] - 
	              m[4] * m[3]  * m[9]  - 
	              m[8] * m[1]  * m[7]  + 
	              m[8] * m[3]  * m[5];

	    inv[15] = m[0] * m[5]  * m[10] - 
	              m[0] * m[6]  * m[9]  - 
	              m[4] * m[1]  * m[10] + 
	              m[4] * m[2]  * m[9]  + 
	              m[8] * m[1]  * m[6]  - 
	              m[8] * m[2]  * m[5];

	    det = m[0] * inv[0] + m[1] * inv[4] + m[2] * inv[8] + m[3] * inv[12];

	    if (det == 0) {
	    	throw "Error : Matrix is not invertible."; 
	    }

	    det = 1.0 / det;

	    for (i = 0; i < 16; i++)
	        inv[i] = inv[i] * det;

	    return inv;
	}

	function mat3x3Inv(m) {
		var a = m.data[0], b = m.data[1],
			c = m.data[2], d = m.data[3],
			e = m.data[4], f = m.data[5],
			g = m.data[6], h = m.data[7],
			i = m.data[8],
			det = a*e*i + b*f*g + c*d*h
					- c*e*g - b*d*i - a*f*h;
		if (det == 0) {
			throw "Not invertible : det is null";
		}

		det = 1.0 / det;

		return [
			(e*i - f*h) * det, (c*h - b*i) * det, (b*f - c*e) * det,
			(f*g - d*i) * det, (a*i - c*g) * det, (c*d - a*f) * det,
			(d*h - e*g) * det, (b*g - a*h) * det, (a*e - b*d) * det
		];
	}

	this.mat4x4Transp = function(m) {
		return [
			m[0], m[4], m[8], 	m[12],
			m[1], m[5], m[9], 	m[13],
			m[2], m[6], m[10], 	m[14],
			m[3], m[7], m[8], 	m[15]
		];
	}

	function addVector(data, offset, v) {
		data[offset]   += v[0];
		data[offset+1] += v[1];
		data[offset+2] += v[2];
	}

	function normalize(data, offset) {
		var x = data[offset],
			y = data[offset+1],
			z = data[offset+2],
			norm = Math.sqrt(x*x + y*y + z*z);
		data[offset]   /= norm;
		data[offset+1] /= norm;
		data[offset+2] /= norm;
	}

	function crossProduct(v1, v2) {
		var v1x = v1[0], v1y = v1[1], v1z = v1[2],
			v2x = v2[0], v2y = v2[1], v2z = v2[2];
		return [
			v1y*v2z - v1z*v2y,
			v1z*v2x - v1x*v2z,
			v1x*v2y - v1y*v2x
		];
	}

	function getV1Pos(positionData, indices, faceFirstIndex) {
		var a = indices[faceFirstIndex]*3,
			b = indices[faceFirstIndex+1]*3;
		return [
			positionData[b]   - positionData[a],
			positionData[b+1] - positionData[a+1],
			positionData[b+2] - positionData[a+2],
		];
	}

	function getV2Pos(positionData, indices, faceFirstIndex) {
		var a = indices[faceFirstIndex]*3,
			b = indices[faceFirstIndex+2]*3;
		return [
			positionData[b]   - positionData[a],
			positionData[b+1] - positionData[a+1],
			positionData[b+2] - positionData[a+2],
		];
	}

	function getV1St(textCoordData, indices, faceFirstIndex) {
		var a = indices[faceFirstIndex]*2,
			b = indices[faceFirstIndex+1]*2;
		return [
			textCoordData[b]   - textCoordData[a],
			textCoordData[b+1] - textCoordData[a+1]
		];
	}

	function getV2St(textCoordData, indices, faceFirstIndex) {
		var a = indices[faceFirstIndex]*2,
			b = indices[faceFirstIndex+2]*2;
		return [
			textCoordData[b]   - textCoordData[a],
			textCoordData[b+1] - textCoordData[a+1]
		];
	}

	function computeNormal(v1, v2) {
		return crossProduct(v1, v2);
	}

	function computeTangent(v1p, v2p, v1st, v2st) {
		var v1stv = v1st[1], v2stv = v2st[1],
			coef = 1.0 / (v1st[0]*v2stv - v2st[0]*v1stv);
		return [
			coef * (v1p[0]*v2stv - v2p[0]*v1stv),
			coef * (v1p[1]*v2stv - v2p[1]*v1stv),
			coef * (v1p[2]*v2stv - v2p[2]*v1stv)
		];
	}

	this.generateNormalAndTangentData = function(positions, textureCoord, indices) {
		if (indices.length % 3 != 0) {
			throw "Only triangular face are supported.";
		}

		//By face
		var normals = new Array(indices.length / 3),
			tangents = new Array(indices.length / 3),
			//By vertex
			n = new Array(positions.length),
			t = new Array(positions.length);

		for (var i=0; i<indices.length/3; i++) {
			var faceIndex = i*3,
				v1 = getV1Pos(positions, indices, faceIndex),
				v2 = getV2Pos(positions, indices, faceIndex);

			normals[i] = computeNormal(v1, v2);
			tangents[i] = 
				computeTangent(v1, v2,
					getV1St(textureCoord, indices, faceIndex),
					getV2St(textureCoord, indices, faceIndex)
				);
		}

		for (var i=0; i<positions.length; i++) {
			n[i] = t[i] = 0.0;
		}

		for (var i=0; i<indices.length/3; i++) {
			var faceIndex = i*3;

			addVector(n, indices[faceIndex]  *3,normals[i]);
			addVector(n, indices[faceIndex+1]*3, normals[i]);
			addVector(n, indices[faceIndex+2]*3, normals[i]);

			addVector(t, indices[faceIndex]   *3,   tangents[i]);
			addVector(t, indices[faceIndex+1] *3, tangents[i]);
			addVector(t, indices[faceIndex+2] *3, tangents[i]);
		}

		for (var i=0; i<n.length; i+=3) {
			normalize(n, i);
			normalize(t, i);
		}

		return {
			normals: n,
			tangents: t
		};
	}

	return this;
})();