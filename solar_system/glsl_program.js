var GLSLProgram = (function() {

	//Deux constantes représentant les type de chacun des deux shaders écrit plus haut.
	//Nous allons nous servir de ces constantes pour savoir si nous avons à faire à un
	//fragment (pixel) shader ou à un vertex (sommet) shader.
	var MIME_FRAGMENT_SHADER = 'x-shader/x-fragment';
	var MIME_VERTEX_SHADER = 'x-shader/x-vertex';

	//Lit du texte dans la structure de la page web.
	//On s'en sert pour charger le code des shaders ecrit en haut de la page.
	//voir plus bas pour plus d'explications.
	function textFromDom(domElement) {
		var text = '',
			currentNode = domElement.firstChild;

		while (currentNode) {
			if (currentNode.nodeType == currentNode.TEXT_NODE) {
				text += currentNode.textContent;
			}

			currentNode = currentNode.nextSibling;
		}

		return text.trim();
	}

	//Charge un shader depuis la structure de la page web et
	//le stock sur la carte graphique.
	function shaderFromDom(gl, domElement) {
		var shader, type;

		if (domElement.nodeName != 'SCRIPT') {
			throw 'domElement parameter should be a <script>, but is ' +
				domElement.nodeName + '.';
		}

		type = domElement.type;

		if (type == MIME_VERTEX_SHADER) {
			shader = gl.createShader(gl.VERTEX_SHADER);
		} else if (type == MIME_FRAGMENT_SHADER) {
			shader = gl.createShader(gl.FRAGMENT_SHADER);
		} else {
			throw 'domElement parameter\'s "type" attribute should be "' + 
				MIME_VERTEX_SHADER + '" or "' + MIME_FRAGMENT_SHADER + '", but is ' + type;
		}

	     //On compile le shader : texte lisible -> code machine
	     //Sinon, la carte graphique ne peut rien en faire.
		gl.shaderSource(shader, textFromDom(domElement));
		gl.compileShader(shader);

	     //On verifie que l'on a pas fait d'erreur en ecrivant le shader.
	     // = on verifie que la compilation a réussi.
		if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
			var errorString = 'Error compiling shader "' + 
								domElement.id == null ? 'Untitled' : domElement.id + '" : \n' +
								gl.getShaderInfoLog(shader);

			gl.deleteShader(shader);					

			throw errorString;
		}

		return shader;
	}

	//Crée un programme graphique en reliant les deux shaders ecrits plus haut.
	function createProgram(gl, vsDomElement, fsDomElement) {
		var vertShader = shaderFromDom(gl, vsDomElement),
			fragShader = shaderFromDom(gl, fsDomElement),
			program = gl.createProgram();

	     //on attache les deux shaders au programme et on le "lie"
	     // = on relie la sortie du vertex shader à l'entrée du fragment shader.
	     //Voir le code des shader plus haut
		gl.attachShader(program, vertShader);
		gl.attachShader(program, fragShader);
		gl.linkProgram(program);

	     //encore une fois, on verifie que l'on a pas fais d'erreur en ecrivant les shader
	     //= on verifie que la liaison a réussi.
		if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
			var errorString = 'Impossible to link shaders "' +
								vsDomElement.id ? 'Untitled' : vsDomElement.id +
								'" et "' + fsDomElement.id ? 'Untitled' : fsDomElement.id +
								'" : \n' + gl.getProgramInfoLog(program);
			gl.deleteProgram(program);
			throw errorString;
		}

		return program;
	}

	function getProgramUniforms(gl, program) {
		var nbUniform = gl.getProgramParameter(program, gl.ACTIVE_UNIFORMS);

		var uniforms = {};

		for (var i=0; i<nbUniform; i++) {
			var info = gl.getActiveUniform(program, i);
			
			var name = info.name;

			if (name.endsWith('[0]')) {
				name = name.substr(0, name.length - 3);
			}

			uniforms[name] = gl.getUniformLocation(program, info.name);
		}

		return uniforms;
	}

	function getProgramAttributes(gl, program) {
		var nbAttributes = gl.getProgramParameter(program, gl.ACTIVE_ATTRIBUTES);

		var attributes = {};

		for (var i=0; i<nbAttributes; i++) {
			var info = gl.getActiveAttrib(program, i);
			
			var name = info.name;

			if (name.endsWith('[0]')) {
				name = name.substr(0, name.length - 3);
			}

			attributes[name] = gl.getAttribLocation(program, info.name);
		}

		return attributes;
	}

	this.create = function (glContext, vsDomElement, fsDomElement) {
		var program = createProgram(glContext, vsDomElement, fsDomElement);

		program.uniforms = getProgramUniforms(glContext, program);
		program.attributes = getProgramAttributes(glContext, program);

		return program;
	}

	return this;

})();