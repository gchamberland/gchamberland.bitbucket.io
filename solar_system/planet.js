class JulianDate {
	constructor(y, M, d, h, m, s, ms) {
		if (M <= 2) {
			M += 12;
			y -= 1;
		}

		var b = Math.floor(y / 400.0) - Math.floor(y / 100.0);

		this.j = Math.floor(365.25 * y) + Math.floor(30.6001 * (M + 1)) + b + 1720996.5 + d
				+ h / 24 + m / 1440 + s / 86400 + ms / 86400000;
	}

	julianCenturySince(otherJulianDate) {
		return (this.j - otherJulianDate.j) / 36525;
	}

	getT() {
		return this.j;
	}
};

JulianDate.fromDate = function(date) {
	return new JulianDate(
		date.getUTCFullYear(),
		date.getUTCMonth()+1,
		date.getUTCDate(),
		date.getUTCHours(),
		date.getUTCMinutes(),
		date.getUTCSeconds(),
		date.getUTCMilliseconds()
	);
};

JulianDate.J2000 = new JulianDate(2000, 1, 1, 12, 0, 0, 0);

class Planet {
	constructor(
		semiMajor, semiMajorVar, eccentricity, eccentricityVar,
		inclination, inclinationVar, meanLongitude, meanLongitudeVar,
		periapsisLongitude, periapsisLongitudeVar, ascendingNodeLongitude, ascendingNodeLongitudeVar,
		b, c, s, f, radius, axialTilt, angSpeedInRadPerJulianCy, rotationAtJ2000
	) {
		this.semiMajor = semiMajor;
		this.semiMajorVar = semiMajorVar;
		this.eccentricity = eccentricity;
		this.eccentricityVar = eccentricityVar;
		this.inclination = inclination;
		this.inclinationVar = inclinationVar;
		this.meanLongitude = meanLongitude;
		this.meanLongitudeVar = meanLongitudeVar;
		this.periapsisLongitude = periapsisLongitude;
		this.periapsisLongitudeVar = periapsisLongitudeVar;
		this.ascendingNodeLongitude = ascendingNodeLongitude;
		this.ascendingNodeLongitudeVar = ascendingNodeLongitudeVar;
		this.b = b;
		this.c = c;
		this.s = s;
		this.f = f;
		this.radius = radius;
		this.axialTilt = axialTilt;
		this.angSpeedInRadPerJulianCy = angSpeedInRadPerJulianCy;
		this.rotationAtJ2000 = rotationAtJ2000;
	}

	getRadius() {
		return this.radius;
	}

	//Unit of the value returned is year if parameters are filled in UA
	getPeriodOfRevolution(julianDate) {
		var t = julianDate.julianCenturySince(JulianDate.J2000);

		var semiMajor = this.semiMajor + this.semiMajorVar * t;
		return Math.sqrt(semiMajor*semiMajor*semiMajor);
	}

	solveKepler(M, e, eps, maxIterations) {
		eps = eps || 1e-6;
		maxIterations = maxIterations || 10;

		//Kepler's equation
		function g(E) {
			return (E - e * Math.sin(E)) - M;
		}

		//Kepler's equation derivative
		function g1(E) {
			return 1 - e * Math.cos(E);
		}

		//Resolve the Kepler's equation with Newton's method.
		var E = M, oldE;
		for (var i=0; i<maxIterations; i++) {
			oldE = E;
			E -= g(E) / g1(E);

			if (Math.abs(E - oldE) < eps) {
				break;
			}
		}

		return E;
	}

	getPosition(julianDate) {
		var t = julianDate.julianCenturySince(JulianDate.J2000);

		var meanLong = this.meanLongitude + this.meanLongitudeVar * t;
		var longPeri = this.periapsisLongitude + this.periapsisLongitudeVar * t;
		var eccentricity = this.eccentricity + this.eccentricityVar * t;
		var semiMajor = this.semiMajor + this.semiMajorVar * t;
		var ascNodeLong = this.ascendingNodeLongitude + this.ascendingNodeLongitudeVar * t;
		var inclination = this.inclination + this.inclinationVar * t;

		var M = meanLong - longPeri + this.b * t*t + this.c * Math.cos(this.f * t) + this.s * Math.sin(this.f * t);
		var argPeri = longPeri - ascNodeLong;

		var E = this.solveKepler(M, eccentricity);

		//Object's position in its orbit plan.
		var x = semiMajor * (Math.cos(E) - eccentricity);
		var y = semiMajor * Math.sqrt(1 - eccentricity*eccentricity) * Math.sin(E);

		//Rotate the orbite plan to get object position in the space.
		var cosArgPeri = Math.cos(argPeri);
		var sinArgPeri = Math.sin(argPeri);
		var cosAscNodeLong = Math.cos(ascNodeLong);
		var sinAscNodeLong = Math.sin(ascNodeLong);

		var A = cosArgPeri * x - sinArgPeri * y;
		var B = sinArgPeri * x + cosArgPeri * y;
		var C = Math.cos(inclination) * B;

		return {
			x : cosAscNodeLong * A - sinAscNodeLong* C,
			y : sinAscNodeLong * A + cosAscNodeLong * C,
			z : Math.sin(inclination) * B,
			latRot : this.axialTilt,
			lonRot : (this.rotationAtJ2000 + this.angSpeedInRadPerJulianCy * t) % (Math.PI * 2)
		};
	}

	getEllipse(julianDate) {
		var t = julianDate.julianCenturySince(JulianDate.J2000);

		var eccentricity = this.eccentricity + this.eccentricityVar * t;
		var semiMajor = this.semiMajor + this.semiMajorVar * t;
		var longPeri = this.periapsisLongitude + this.periapsisLongitudeVar * t;
		var ascNodeLong = this.ascendingNodeLongitude + this.ascendingNodeLongitudeVar * t;
		var inclination = this.inclination + this.inclinationVar * t;

		var argPeri = longPeri - ascNodeLong;

		//Object's trajectory in its orbit plan (=ellipse defined by center O, point a, and point b).
		//Oy and ay are null.
		var ox = -semiMajor*eccentricity
		var ax = ox + semiMajor;
		var bx = ox;
		var by = semiMajor * Math.sqrt(1 - eccentricity*eccentricity);

		//Rotate orbit plan to get trajectory in the space.
		var cosArgPeri = Math.cos(argPeri);
		var sinArgPeri = Math.sin(argPeri);
		var cosAscNodeLong = Math.cos(ascNodeLong);
		var sinAscNodeLong = Math.sin(ascNodeLong);

		var A = cosArgPeri * ox;
		var B = sinArgPeri * ox;
		var C = Math.cos(inclination) * B;

		var o = {
			x : cosAscNodeLong * A - sinAscNodeLong * C,
			y : sinAscNodeLong * A + cosAscNodeLong * C,
			z : Math.sin(inclination) * B
		};

		A = cosArgPeri * ax;
		B = sinArgPeri * ax;
		C = Math.cos(inclination) * B;

		var a = {
			x : cosAscNodeLong * A - sinAscNodeLong * C,
			y : sinAscNodeLong * A + cosAscNodeLong * C,
			z : Math.sin(inclination) * B
		};

		A = cosArgPeri * bx - sinArgPeri * by;
		B = sinArgPeri * bx + cosArgPeri * by;
		C = Math.cos(inclination) * B;

		var b = {
			x : cosAscNodeLong * A - sinAscNodeLong * C,
			y : sinAscNodeLong * A + cosAscNodeLong * C,
			z : Math.sin(inclination) * B
		};

		return {
			o: o,
			a: a,
			b: b
		};
	}

	getDistanceFrom(planet, julianDate) {
		var p1 = this.getPosition(julianDate, julianDate);
		var p2 = planet.getPosition(julianDate, julianDate);
		var dx = p1.x - p2.x;
		var dy = p1.y - p2.y;
		var dz = p1.z - p2.z;
		return Math.sqrt(dx*dx + dy*dy + dz*dz);
	}
};